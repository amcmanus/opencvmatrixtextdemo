#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <sstream>
#include <string>
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>

using namespace cv;
using namespace std;

#define W 65
#define H 65

Mat matrixOut(Mat in);
Mat colourIn(Mat in, int x, int y);
std::string charArray[W][H];
int scalArray[W][H];

int main(int argc, char *argv[]){
	
	VideoCapture cap(0);
	for(unsigned i = 0; i < W; i++){
		for(unsigned j = 0; j < H; j++){
			charArray[i][j] = "";
			scalArray[i][j] = 0;
		}
	}
	if(!cap.isOpened()){
		cout << "Could not open Capture Device" << endl;
		return -1;
		}
		
		cap.set(CV_CAP_PROP_FRAME_WIDTH, 1920);
		 cap.set(CV_CAP_PROP_FRAME_HEIGHT, 1080);
		
		namedWindow("out", 1);
		std::string chars(
        "abcdefghijklmnopqrstuvwxyz"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "1234567890"
        "!@#$%^&*()"
        "`~-_=+[{]{\\|;:'\",<.>/? ");
    boost::random::random_device rng;
    boost::random::uniform_int_distribution<> index_dist(0, chars.size() - 1);
		for(;;){
			Mat frame;
			cap >> frame;
			imshow("out", frame);
			Mat show = matrixOut(frame);
			for(int y = H-1; y>0; y--){
				for(int x=0; x<W;x++){
					if(charArray[x][y-1] == ""){
						charArray[x][y] = "";
					}else{
						charArray[x][y] = charArray[x][y-1];
						
					}
				}
			}
			for(int x=0; x<W; x++){
				srand(time(NULL));
				int chance = 1+rand()%10;
				if(chance == 5){
					charArray[x][0] = chars[index_dist(rng)];
				}else if(charArray[x][0] != ""){
					charArray[x][0] = chars[index_dist(rng)];
				}else{
					charArray[x][0] = "";
				}
			}
			for(int x = 0;x<W;x++){
				for(int y=0;y<H;y++){
					string text = charArray[x][y];
					putText(show, text, Point(x*show.cols/H+show.cols/H,y*show.rows/W+show.rows/W),FONT_HERSHEY_SIMPLEX, .5, Scalar(0,scalArray[x][y],0),1,4);
				}
			}
			imshow("temp", show);
			if(waitKey(30) >=0) break;
		}
	return 0;
}

Mat matrixOut(Mat in){
	Mat out = Mat::zeros(in.rows, in.cols, CV_8UC3); 
	Mat grey = Mat::zeros(in.rows, in.cols, CV_8UC3);
	cvtColor(in, grey, CV_RGB2GRAY);
	Mat seg = Mat::zeros(grey.rows, grey.cols, CV_8UC3);
	bool check = false;
	for(int y = 0; y<H; y++){
		for(int x=0;x<W;x++){
			if(check!=true){
			seg = colourIn(grey, x, y);
			check = true;
			}else{
			seg = colourIn(seg, x, y);	
			}
		}
	}
	imshow("seg", seg);
	return out;
}	
	
Mat colourIn(Mat temp, int x, int y){
	Rect roi = Rect(x*temp.cols/H,y*temp.rows/W,temp.cols/W,temp.rows/H);
	Mat image_roi = temp(roi);
	Scalar c = mean(image_roi);
	rectangle(temp, roi, c, -1,0,0);
	scalArray[x][y] = c.val[0];
	return temp;
}	
