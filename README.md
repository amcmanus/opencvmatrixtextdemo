# README
# This is a pseudo version of the Terminal code in The Matrix movies.
# 
# You require Boost and OpenCV libraries to run this code.
#
# If OpenCV and Boost are set up, compile with `g++ matrixtext.cpp -o matrixtext "pkg-config opencv --cflags --libs" -I ../Tools/boost_1_51_0 -L ../Tools/boost_1_51_0/libs/random/ -lboost_random`
#
# If you have any issue, please leave a wiki note on this repo.
